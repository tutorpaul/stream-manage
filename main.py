"""
Polls the tutorpaul API for info about donations, writes most recent and
top donation to a file for display with OBS
"""
import os
from time import sleep
import requests
from datetime import date
from decimal import Decimal


API_TOKEN = os.environ.get("TUTORPAUL_API_TOKEN")
TEMPLATE = """Top donation: {top[donor_name]} -- ${top[amount]}
Last donation: {last[donor_name]} -- ${last[amount]}
Total donations during stream: ${total_amount}"""


class StreamSetup:
    def __init__(self):
        # create output files for
        # date
        self.to_file(date.today().strftime("%B %d, %Y"), "date.txt")
        # course
        _course = input("What course? (MEEN 364) : ")
        course = _course.strip()
        if not course:
            course = "MEEN 364"
        self.to_file(course, "header.txt")
        # subject
        _material = input("What material? ")
        material = _material.strip()
        self.to_file(material, "title.txt")

    def to_file(self, content, filename):
        with open(filename, "w") as f:
            f.write(content)


class DonationWatcher:
    output_filename = "donation_data.txt"
    top_donation = None
    latest_donation = None
    latest_parsed_pk = 0
    total_amount = Decimal(0)

    def __init__(self):
        self.update(silent=True)
        self.total_amount = Decimal(0)
        self.update_file()

    def get_new_donations(self):
        response = requests.get(
            f"https://www.tutorpaul.com/api/v1/donations/?id__gt={self.latest_parsed_pk}",
            headers={"Authorization": f"Token {API_TOKEN}"},
        )
        # print(response)
        return response.json()

    def parse_donations(self, new_donations, *, silent):
        if not new_donations:
            return False
        self.latest_donation = new_donations[0]
        self.latest_parsed_pk = self.latest_donation["pk"]
        if self.top_donation is None:
            self.top_donation = self.latest_donation
        for donation in new_donations:
            if not silent:
                print(f"{donation['donor_name']} -- {donation['amount']}")
            donation["amount"] = Decimal(donation["amount"])
            self.total_amount += donation["amount"]
            if donation["amount"] > self.top_donation["amount"]:
                self.top_donation = donation
        return True

    def update_file(self):
        filedata = TEMPLATE.format(
            top=self.top_donation,
            last=self.latest_donation,
            total_amount=self.total_amount,
        )
        with open(self.output_filename, "w") as f:
            f.write(filedata)

    def update(self, silent=False):
        new_donations = self.get_new_donations()
        if self.parse_donations(new_donations, silent=silent):
            self.update_file()


if __name__ == "__main__":
    StreamSetup()
    watcher = DonationWatcher()
    while True:
        watcher.update()
        sleep(10)
